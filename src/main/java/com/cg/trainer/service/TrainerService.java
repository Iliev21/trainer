package com.cg.trainer.service;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.cg.trainer.entity.Trainer;
import com.cg.trainer.repository.TrainerRepository;

@Service
public class TrainerService {
	
	private TrainerRepository trainerRepository;
	
	public TrainerService(TrainerRepository trainerRepository) {
		this.trainerRepository = trainerRepository;
	}

	//TODO: Add java-doc
	public Trainer create(Trainer trainer) {
		//TODO: Add validation that there isn't already a trainer with such an email
		return trainerRepository.save(trainer);
	}
	
	public Trainer find(long id) {
		Optional<Trainer> trainer = trainerRepository.findById(id);
		if(trainer.isPresent()) {
			return trainer.get();
		}
		throw new NoSuchElementException(String.format(
				"Trainer with id %d is missing in the database", id));
	}
}
