package com.cg.trainer.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import com.cg.trainer.dto.TrainerDTO;
import com.cg.trainer.entity.Trainer;
import com.cg.trainer.exception.IncorrectTrainerIdException;

@Mapper(componentModel = "spring")
public interface TrainerMapper {
	
	static final String TRAINER_PREFIX = "trainer-id-";
	static final String REGEX_TRAINER_PREFIX = TRAINER_PREFIX + "%06d";
	
	Trainer trainerDTOtoTrainerWithoutId(TrainerDTO trainerDTO);

	@Mapping(source = "id", target = "id", qualifiedByName = "idStringToId")
	Trainer trainerDTOtoTrainer(TrainerDTO trainerDTO);
	
	@Mapping(source = "id", target = "id", qualifiedByName = "idToIdString")
	TrainerDTO trainerToTrainerDTO(Trainer trainer);
	
	@Named("idStringToId")
	public static long idStringToId(String trainerIdString) throws IncorrectTrainerIdException {
		if(trainerIdString != null && !trainerIdString.isEmpty() &&
				trainerIdString.startsWith(TRAINER_PREFIX)) {
			return Long.parseLong(trainerIdString.replace(TRAINER_PREFIX, ""));
		}
		//TODO: Add custom handling of Exceptions
		//TODO: Move text files to translation bundles
		throw new IncorrectTrainerIdException("Trainer id query parameter needs to follow the pattern " + TRAINER_PREFIX + 
				"XXXXXX. Where XXXXXX should be a number");
	}
	
	@Named("idToIdString")
	public static String idToIdString(long trainerId) {
		return String.format(REGEX_TRAINER_PREFIX, trainerId);
	}
	
}
