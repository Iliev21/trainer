package com.cg.trainer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cg.trainer.entity.Trainer;

public interface TrainerRepository extends JpaRepository<Trainer, Long>{

}
