package com.cg.trainer.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@SuppressWarnings("unused")
public class TrainerDTO {

	private String id;
	
	@Email
	@NotNull
	@JsonProperty("email")
	private String email;
	
	//TODO: Create custom validator for phone number
	@NotNull
	@JsonProperty("phone")
	private String phone;

	@NotNull
	@JsonProperty("first_name")
	private String firstName;
	
	@NotNull
	@JsonProperty("last_name")
	private String lastName;
}
