package com.cg.trainer.exception;

public class IncorrectTrainerIdException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public IncorrectTrainerIdException(String message) {
		super(message);
	}

}
