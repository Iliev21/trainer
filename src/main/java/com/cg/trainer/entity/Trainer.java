package com.cg.trainer.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
@SuppressWarnings("unused")
public class Trainer {

	@Id
	@GeneratedValue
	private long id;
	private String email;
	private String phone;
	private String firstName;
	private String lastName;
}
