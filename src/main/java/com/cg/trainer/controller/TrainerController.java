package com.cg.trainer.controller;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cg.trainer.dto.TrainerDTO;
import com.cg.trainer.entity.Trainer;
import com.cg.trainer.exception.IncorrectTrainerIdException;
import com.cg.trainer.mapper.TrainerMapper;
import com.cg.trainer.service.TrainerService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "trainer", produces = MediaType.APPLICATION_JSON_VALUE)
//TODO: Add Swagger documentation
public class TrainerController {
	
	private TrainerMapper trainerMapper;
	private TrainerService trainerService;
	
	public TrainerController(TrainerMapper trainerMapper,
		TrainerService trainerService) {
		this.trainerMapper = trainerMapper;
		this.trainerService = trainerService;
	}
	
	@PostMapping
	@ApiOperation("Create a new Trainer")
	public TrainerDTO create(@ApiParam("new Trainer information")
			@Valid @RequestBody TrainerDTO trainerDTO) {
		Trainer trainer = trainerMapper.trainerDTOtoTrainerWithoutId(trainerDTO);
		Trainer createdTrainer = trainerService.create(trainer);
		return trainerMapper.trainerToTrainerDTO(createdTrainer);
	}
	
	@GetMapping
	public TrainerDTO getTrainer(@RequestParam("id") String trainerIdString) throws IncorrectTrainerIdException {
		long trainerId = TrainerMapper.idStringToId(trainerIdString);
			Trainer trainer = trainerService.find(trainerId);
			return trainerMapper.trainerToTrainerDTO(trainer);
	}
	
}
