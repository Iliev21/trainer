# Purpose

This is a sample project created for CampGladiator.
	The purpose of this project is to demonstrate the creation of a basic CRUD API.

# Current State

The current state of the project gives the capabilities to:
- create trainers based on the input provided by the consumer of the API
- Retrieve existing trainers based on their trainer id

# Technologies
- **Spring Boot**
- **Hibernate**
- **Spring Data**
- **Swagger**
- **Lombok**
- **MapStruct**
- **H2 DB** - for the first version of the project I've used an in memory database. 


# Project setup
### Prerequisites
- Install Java 11
- Install Maven

### Steps to run
- Open terminal
- Run ```maven clean install```
- Run ```./mvnw spring-boot:run```
- Use Swagger/Postman/Curl to test the app

# Execution
## Creation of Trainer
To create a Trainer you need to make a POST request to ```http://localhost:8080/trainer```
The request body should be as follows:
``` 
{
    "id" : "trainer-id-000001",
    "email" : "trainer@campgladiator.com",
    "phone" : "5125125120",
    "first_name": "Fearless",
    "last_name": "Contender"
}
```

### Retrieve trainers

To test the fetching of a Trainer you can do a request to ```http://localhost:8080/trainer?id=trainer-id-000001``` where **trainer-id-XXXXXX** is the number of the trainer.

# Pending work

Since the time for completing this task was limited I am listing some of the **TODO** tasks that are pending:

- Add a persistent database and use H2 only for testing purposes.
- Add java-doc
- Add different profiles for when running the application so we can switch from develop to test, prod etc.
- Add documentation for Swagger so it is easier for FE developers to consume the API
- Add meaningful messages for validation messages.
- Create custom validators for the case where a customer is trying to register a Trainer with existing email/phone.
- Add integration/unit tests
- Add custom handling of exceptions and return human readable error messages with appropriate status code.
- If internationalization is needed think if we should extract text to property files
- Discuss if we need authentication to the app etc.
- Improve the README of the repo...
